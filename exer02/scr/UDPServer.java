import java.net.*;
import java.io.*;

/*
    UDP Server

    @author cedido po Bruno Sousa
    @version 1.0
*/
public class UDPServer{
    private final static int BIND_PORT = 6789;

    //TODO: To complete
    /* note that this method is incomplete, missing arguments,  */
    public static String treatClients(){return "";}

    public static void main(String args[]){
        DatagramSocket aSocket = null;
        try{
            aSocket = new DatagramSocket(BIND_PORT);
            // create socket at agreed port
            byte[] buffer = new byte[1000];
            while(true){
                DatagramPacket request = new DatagramPacket(buffer, buffer.length);
                aSocket.receive(request);
                DatagramPacket reply = new DatagramPacket(request.getData(), request.getLength(),
                        request.getAddress(), request.getPort());
                aSocket.send(reply);
            }
        }catch (SocketException e){System.out.println("Socket: " + e.getMessage());
        }catch (IOException e) {System.out.println("IO: " + e.getMessage());
        }finally {if(aSocket != null) aSocket.close();}
    }
}
