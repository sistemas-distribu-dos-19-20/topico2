import java.io.RandomAccessFile;

/*
    PipeServer class to read from Pipe
 */
public class PipeServer {
    private String namePipeIn;
    private String namePipeOut;

    public PipeServer(String in, String out_) {
        namePipeIn = in;
        namePipeOut = out_;
    }

    public String treatInput(String in){
        //TODO: Make the logic of this method


        return Math.random() + " CHANGME";
    }

    public static void main(String[] args) {

        /*
            To execute:

            java PipeServer NAME_IN_PIPE NAME_OUT_PIPE
         */

        //TODO: Add verification of arguments at cmd
        System.out.println("PArameters" + args[0] + " " + args[1]);
        PipeServer mypipeServer = new PipeServer(args[0], args[1]);

        String aux_input, aux_result;
        RandomAccessFile pipe_read = null, pipe_write = null;

        // Connect to the named pipe

        try {
            pipe_read = new RandomAccessFile(mypipeServer.getNamePipeIn(), "r");
            pipe_write = new RandomAccessFile(mypipeServer.getNamePipeOut(), "rw");

            //while(true) {


                // REad input
                try {
                    //TODO: Check if it has messages from clients

                    aux_input = pipe_read.readLine();
                    aux_result = mypipeServer.treatInput(aux_input);

                    //write back the result
                    System.out.println("Processed the input " + aux_input + " = " + aux_result);
                    pipe_write.write(aux_result.getBytes());

                    Thread.sleep(10000); // Again not efficient

                } catch (Exception e) {
                    //break;
                }

            //}
            pipe_read.close();
            pipe_write.close();


        }catch(Exception e) {
            //TODO: Process error on opening of closing pipes
            e.printStackTrace();
        }

    }


    // Get and Sets
    public String getNamePipeIn() {
        return namePipeIn;
    }

    public void setNamePipeIn(String namePipeIn) {
        this.namePipeIn = namePipeIn;
    }

    public String getNamePipeOut() {
        return namePipeOut;
    }

    public void setNamePipeOut(String namePipeOut) {
        this.namePipeOut = namePipeOut;
    }
}
