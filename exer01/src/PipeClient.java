import java.io.RandomAccessFile;

/*
    Pipe Client

    @author cedido por Bruno Sousa
    @version 1.0
*/
public class PipeClient {
    private String namePipeIn;
    private String namePipeOut;

    public PipeClient(String namePipeIn, String namePipeOut) {
        this.namePipeIn = namePipeIn;
        this.namePipeOut = namePipeOut;
    }



    public static void main(String[] args) {
        /*
            To execute:

            java PipieClient NAME_IN_PIPE NAME_OUT_PIPE
         */

        //TODO: Add verification of arguments at cmd
        PipeClient mypipeClient = new PipeClient(args[0], args[1]);

        String aux_result, aux_msg_to_server ;
        RandomAccessFile pipe_read = null, pipe_write = null;

        // Connect to the named pipe

        try {
            pipe_write = new RandomAccessFile(mypipeClient.getNamePipeOut(), "rw");
            pipe_read = new RandomAccessFile(mypipeClient.getNamePipeIn(), "r");

            // REad input
            try {
                //TODO: Ask input to the user
                aux_msg_to_server="CHANGME_CLIENT";

                // Send message to server
                pipe_write.write(aux_msg_to_server.getBytes());


                //TODO: change to wait for server result, check if pipe has some data
                Thread.sleep(1000); //Sleep This is not efficient.

                aux_result = pipe_read.readLine();

                System.out.println("Server returned " + aux_result + " to the msg " + aux_msg_to_server);

                //TODO: After presenting the message clear pipes...

            } catch (Exception e) {

            }

            pipe_read.close();
            pipe_write.close();

        }catch(Exception e){
            //TODO: process possible errors
        }
    }



    public String getNamePipeIn() {
        return namePipeIn;
    }

    public void setNamePipeIn(String namePipeIn) {
        this.namePipeIn = namePipeIn;
    }

    public String getNamePipeOut() {
        return namePipeOut;
    }

    public void setNamePipeOut(String namePipeOut) {
        this.namePipeOut = namePipeOut;
    }
}
